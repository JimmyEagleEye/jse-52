package ru.korkmasov.tsc.exception.other;

import ru.korkmasov.tsc.exception.AbstractException;

import org.jetbrains.annotations.NotNull;

public class ServiceLocatorNotFoundException extends AbstractException {

    @NotNull
    private static final String MESSAGE = "Service locator not found!";

    public ServiceLocatorNotFoundException() {
        super(MESSAGE);
    }

}
