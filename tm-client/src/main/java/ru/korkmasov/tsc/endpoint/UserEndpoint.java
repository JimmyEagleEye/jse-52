package ru.korkmasov.tsc.endpoint;

import javax.management.relation.Role;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "user", propOrder = {
        "login",
        "passwordHash",
        "email",
        "firstName",
        "lastName",
        "middleName",
        "role",
        "locked"
})
public class UserEndpoint
        extends AbstractEntity {

    protected String login;
    protected String passwordHash;
    protected String email;
    protected String firstName;
    protected String lastName;
    protected String middleName;
    @XmlSchemaType(name = "string")
    protected Role role;
    protected boolean locked;

    public String getLogin() {
        return login;
    }

    public void setLogin(String value) {
        this.login = value;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String value) {
        this.passwordHash = value;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String value) {
        this.email = value;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String value) {
        this.firstName = value;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String value) {
        this.lastName = value;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String value) {
        this.middleName = value;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role value) {
        this.role = value;
    }

    public boolean isLocked() {
        return locked;
    }

    public void setLocked(boolean value) {
        this.locked = value;
    }

}
