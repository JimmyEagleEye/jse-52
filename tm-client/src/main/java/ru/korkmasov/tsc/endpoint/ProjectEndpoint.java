package ru.korkmasov.tsc.endpoint;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebResult;
import javax.jws.WebService;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.ws.Action;
import javax.xml.ws.RequestWrapper;
import javax.xml.ws.ResponseWrapper;

@WebService(targetNamespace = "http://endpoint.tsc.korkmasov.ru/", name = "ProjectEndpoint")
@XmlSeeAlso({ObjectFactory.class})
public interface ProjectEndpoint {

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/removeProjectByNameRequest", output = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/removeProjectByNameResponse")
    @RequestWrapper(localName = "removeProjectByName", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.RemoveProjectByName")
    @ResponseWrapper(localName = "removeProjectByNameResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.RemoveProjectByNameResponse")
    public void removeProjectByName(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "name", targetNamespace = "")
                    java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/addProjectAllRequest", output = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/addProjectAllResponse")
    @RequestWrapper(localName = "addProjectAll", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.AddProjectAll")
    @ResponseWrapper(localName = "addProjectAllResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.AddProjectAllResponse")
    public void addProjectAll(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "collection", targetNamespace = "")
                    java.util.List<ru.korkmasov.tsc.endpoint.Project> collection
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/findProjectAllRequest", output = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/findProjectAllResponse")
    @RequestWrapper(localName = "findProjectAll", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FindProjectAll")
    @ResponseWrapper(localName = "findProjectAllResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FindProjectAllResponse")
    @WebResult(name = "return", targetNamespace = "")
    public java.util.List<ru.korkmasov.tsc.endpoint.Project> findProjectAll(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/startProjectByIndexRequest", output = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/startProjectByIndexResponse")
    @RequestWrapper(localName = "startProjectByIndex", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.StartProjectByIndex")
    @ResponseWrapper(localName = "startProjectByIndexResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.StartProjectByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Project startProjectByIndex(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "index", targetNamespace = "")
                    java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/findProjectByIndexRequest", output = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/findProjectByIndexResponse")
    @RequestWrapper(localName = "findProjectByIndex", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FindProjectByIndex")
    @ResponseWrapper(localName = "findProjectByIndexResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FindProjectByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Project findProjectByIndex(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "index", targetNamespace = "")
                    java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/clearProjectRequest", output = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/clearProjectResponse")
    @RequestWrapper(localName = "clearProject", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.ClearProject")
    @ResponseWrapper(localName = "clearProjectResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.ClearProjectResponse")
    public void clearProject(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/updateProjectByIdRequest", output = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/updateProjectByIdResponse")
    @RequestWrapper(localName = "updateProjectById", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.UpdateProjectById")
    @ResponseWrapper(localName = "updateProjectByIdResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.UpdateProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Project updateProjectById(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id,
            @WebParam(name = "name", targetNamespace = "")
                    java.lang.String name,
            @WebParam(name = "description", targetNamespace = "")
                    java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/removeProjectRequest", output = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/removeProjectResponse")
    @RequestWrapper(localName = "removeProject", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.RemoveProject")
    @ResponseWrapper(localName = "removeProjectResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.RemoveProjectResponse")
    public void removeProject(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "entity", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Project entity
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/updateProjectByIndexRequest", output = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/updateProjectByIndexResponse")
    @RequestWrapper(localName = "updateProjectByIndex", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.UpdateProjectByIndex")
    @ResponseWrapper(localName = "updateProjectByIndexResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.UpdateProjectByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Project updateProjectByIndex(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "index", targetNamespace = "")
                    java.lang.Integer index,
            @WebParam(name = "name", targetNamespace = "")
                    java.lang.String name,
            @WebParam(name = "description", targetNamespace = "")
                    java.lang.String description
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/removeProjectByIndexRequest", output = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/removeProjectByIndexResponse")
    @RequestWrapper(localName = "removeProjectByIndex", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.RemoveProjectByIndex")
    @ResponseWrapper(localName = "removeProjectByIndexResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.RemoveProjectByIndexResponse")
    public void removeProjectByIndex(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "index", targetNamespace = "")
                    java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/findProjectByIdRequest", output = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/findProjectByIdResponse")
    @RequestWrapper(localName = "findProjectById", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FindProjectById")
    @ResponseWrapper(localName = "findProjectByIdResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FindProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Project findProjectById(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/findProjectByNameRequest", output = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/findProjectByNameResponse")
    @RequestWrapper(localName = "findProjectByName", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FindProjectByName")
    @ResponseWrapper(localName = "findProjectByNameResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FindProjectByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Project findProjectByName(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "name", targetNamespace = "")
                    java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/finishProjectByNameRequest", output = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/finishProjectByNameResponse")
    @RequestWrapper(localName = "finishProjectByName", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FinishProjectByName")
    @ResponseWrapper(localName = "finishProjectByNameResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FinishProjectByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Project finishProjectByName(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "name", targetNamespace = "")
                    java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/finishProjectByIndexRequest", output = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/finishProjectByIndexResponse")
    @RequestWrapper(localName = "finishProjectByIndex", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FinishProjectByIndex")
    @ResponseWrapper(localName = "finishProjectByIndexResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FinishProjectByIndexResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Project finishProjectByIndex(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "index", targetNamespace = "")
                    java.lang.Integer index
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/finishProjectByIdRequest", output = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/finishProjectByIdResponse")
    @RequestWrapper(localName = "finishProjectById", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FinishProjectById")
    @ResponseWrapper(localName = "finishProjectByIdResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.FinishProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Project finishProjectById(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/removeProjectByIdRequest", output = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/removeProjectByIdResponse")
    @RequestWrapper(localName = "removeProjectById", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.RemoveProjectById")
    @ResponseWrapper(localName = "removeProjectByIdResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.RemoveProjectByIdResponse")
    public void removeProjectById(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "projectId", targetNamespace = "")
                    java.lang.String projectId
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/startProjectByIdRequest", output = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/startProjectByIdResponse")
    @RequestWrapper(localName = "startProjectById", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.StartProjectById")
    @ResponseWrapper(localName = "startProjectByIdResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.StartProjectByIdResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Project startProjectById(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "id", targetNamespace = "")
                    java.lang.String id
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/startProjectByNameRequest", output = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/startProjectByNameResponse")
    @RequestWrapper(localName = "startProjectByName", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.StartProjectByName")
    @ResponseWrapper(localName = "startProjectByNameResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.StartProjectByNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Project startProjectByName(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "name", targetNamespace = "")
                    java.lang.String name
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/addProjectRequest", output = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/addProjectResponse")
    @RequestWrapper(localName = "addProject", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.AddProject")
    @ResponseWrapper(localName = "addProjectResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.AddProjectResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Project addProject(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "entity", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Project entity
    );

    @WebMethod
    @Action(input = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/addProjectWithNameRequest", output = "http://endpoint.tsc.korkmasov.ru/ProjectEndpoint/addProjectWithNameResponse")
    @RequestWrapper(localName = "addProjectWithName", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.AddProjectWithName")
    @ResponseWrapper(localName = "addProjectWithNameResponse", targetNamespace = "http://endpoint.tsc.korkmasov.ru/", className = "ru.korkmasov.tsc.endpoint.AddProjectWithNameResponse")
    @WebResult(name = "return", targetNamespace = "")
    public ru.korkmasov.tsc.endpoint.Project addProjectWithName(

            @WebParam(name = "session", targetNamespace = "")
                    ru.korkmasov.tsc.endpoint.Session session,
            @WebParam(name = "name", targetNamespace = "")
                    java.lang.String name,
            @WebParam(name = "description", targetNamespace = "")
                    java.lang.String description
    );
}
