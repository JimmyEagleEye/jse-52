package ru.korkmasov.tsc.command.user;

import ru.korkmasov.tsc.exception.entity.UserNotFoundException;
import ru.korkmasov.tsc.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.exception.entity.UserNotFoundException;

public class UserLoginCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-login";
    }

    @Override
    public String description() {
        return "Log in to the system";
    }

    @Override
    public void execute() {
        //if (serviceLocator.getSession() != null) throw new AlreadyLoggedInException();
        System.out.println("[LOGIN]");
        System.out.println("ENTER LOGIN:");
        //@NotNull final String login = nextLine();
        //if (!serviceLocator.getUserEndpoint().existsUserByLogin(login)) throw new UserNotFoundException();
        System.out.println("ENTER PASSWORD:");
        //serviceLocator.setSession(serviceLocator.getSessionEndpoint().openSession(login, TerminalUtil.nextLine()));
    }
}
