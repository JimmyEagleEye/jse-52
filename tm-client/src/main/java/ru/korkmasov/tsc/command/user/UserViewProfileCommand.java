package ru.korkmasov.tsc.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.endpoint.UserEndpoint;
import ru.korkmasov.tsc.model.User;

public final class UserViewProfileCommand extends AbstractUserCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "user-view";
    }

    @Override
    public String description() {
        return "Show user profile";
    }

    @Override
    public void execute() {
        //@Nullable final User user = serviceLocator.getAdminEndpoint().viewUser(serviceLocator.getSession());
        //if (user == null) return;
        //showUser(user);
    }

}
