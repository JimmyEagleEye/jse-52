package ru.korkmasov.tsc.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class TaskClearCommand extends AbstractTaskCommand {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "task-clear";
    }

    @NotNull
    @Override
    public String description() {
        return "Clear all tasks";
    }

    @Override
    public void execute() {
        serviceLocator.getTaskEndpoint().clearTask(serviceLocator.getSession());
    }
}
