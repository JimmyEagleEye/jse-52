package ru.korkmasov.tsc.command.project;

import ru.korkmasov.tsc.endpoint.Project;
import ru.korkmasov.tsc.exception.entity.ProjectNotFoundException;
import ru.korkmasov.tsc.util.TerminalUtil;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectByNameRemoveCommand extends AbstractProjectCommand {

    @Override
    public @Nullable String arg() {
        return null;
    }

    @Override
    public @NotNull String name() {
        return "project-remove-by-name";
    }

    @Override
    public @NotNull String description() {
        return "Remove project by name";
    }

    @Override
    public void execute() {
        System.out.println("Enter name");
        @Nullable final String name = TerminalUtil.nextLine();
        @Nullable final Project project = serviceLocator.getProjectEndpoint().findProjectByName(serviceLocator.getSession(), name);
        if (project == null) throw new ProjectNotFoundException();
        serviceLocator.getProjectEndpoint().removeProjectByName(serviceLocator.getSession(), name);
    }

}
