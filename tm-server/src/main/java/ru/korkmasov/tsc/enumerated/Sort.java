package ru.korkmasov.tsc.enumerated;

import ru.korkmasov.tsc.comparator.ComparatorByCreated;
import ru.korkmasov.tsc.comparator.ComparatorByDateStart;
import ru.korkmasov.tsc.comparator.ComparatorByName;

import java.util.Comparator;

public enum Sort {

    NAME("Sort by name", ComparatorByName.getInstance()),
    CREATED("Sort by created", ComparatorByCreated.getInstance()),
    DATE_START("Sort by start date", ComparatorByDateStart.getInstance()),
    STATUS("Sort by status", ComparatorByCreated.getInstance());

    private final String displayName;

    protected final Comparator comparator;

    Sort(String displayName, Comparator comparator) {
        this.displayName = displayName;
        this.comparator = comparator;
    }

    public Comparator getComparator() {
        return comparator;
    }

    public String getDisplayName() {
        return displayName;
    }


}