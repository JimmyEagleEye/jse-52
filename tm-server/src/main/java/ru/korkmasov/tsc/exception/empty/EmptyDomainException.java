package ru.korkmasov.tsc.exception.empty;

import ru.korkmasov.tsc.exception.AbstractException;

import org.jetbrains.annotations.NotNull;

public class EmptyDomainException extends AbstractException {

    @NotNull
    public EmptyDomainException() {
        super("Error. DomainDTO is empty");
    }

}
