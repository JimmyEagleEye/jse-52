package ru.korkmasov.tsc.comparator;

import ru.korkmasov.tsc.api.entity.IHasName;

import java.util.Comparator;

public final class ComparatorByName implements Comparator<IHasName> {

    private final static ComparatorByName INSTANCE = new ComparatorByName();

    private ComparatorByName() {
    }

    public static ComparatorByName getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(IHasName o1, IHasName o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getName().compareTo(o2.getName());
    }

}
