package ru.korkmasov.tsc.comparator;

import ru.korkmasov.tsc.api.entity.IHasStartDate;

import java.util.Comparator;

public class ComparatorByDateStart implements Comparator<IHasStartDate> {

    private final static ComparatorByDateStart INSTANCE = new ComparatorByDateStart();

    private ComparatorByDateStart() {
    }

    public static ComparatorByDateStart getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(IHasStartDate o1, IHasStartDate o2) {
        if (o1 == null || o2 == null) return 0;
        return o1.getStartDate().compareTo(o2.getStartDate());
    }

}
