package ru.korkmasov.tsc.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.api.IRecordService;
import ru.korkmasov.tsc.dto.SessionRecord;
import ru.korkmasov.tsc.dto.UserRecord;
import ru.korkmasov.tsc.enumerated.Role;

import java.util.List;

public interface ISessionRecordService extends IRecordService<SessionRecord> {

    SessionRecord open(@Nullable String login, @Nullable String password);

    UserRecord checkDataAccess(@Nullable String login, @Nullable String password);

    void validate(@NotNull SessionRecord session, Role role);

    void validate(@Nullable SessionRecord session);

    SessionRecord sign(@Nullable SessionRecord session);

    void close(@Nullable SessionRecord session);

    void closeAllByUserId(@Nullable String userId);

    @Nullable List<SessionRecord> findAllByUserId(@Nullable String userId);
}
