package ru.korkmasov.tsc.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.korkmasov.tsc.api.IRecordService;
import ru.korkmasov.tsc.api.service.IConnectionService;
import ru.korkmasov.tsc.dto.AbstractRecord;

public abstract class AbstractRecordService<E extends AbstractRecord> implements IRecordService<E> {

    @NotNull
    protected final IConnectionService connectionService;

    @NotNull
    public AbstractRecordService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}
