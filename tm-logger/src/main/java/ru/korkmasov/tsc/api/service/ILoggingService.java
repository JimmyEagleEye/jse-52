package ru.korkmasov.tsc.api.service;

import ru.korkmasov.tsc.dto.LoggerDTO;

public interface ILoggingService {

    void writeLog(LoggerDTO message);

}
