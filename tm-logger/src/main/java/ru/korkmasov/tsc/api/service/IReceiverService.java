package ru.korkmasov.tsc.api.service;


import javax.jms.MessageListener;

public interface IReceiverService {

    void receive(MessageListener listener);

}
