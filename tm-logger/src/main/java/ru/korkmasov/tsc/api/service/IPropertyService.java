package ru.korkmasov.tsc.api.service;

import org.jetbrains.annotations.Nullable;

public interface IPropertyService {

    @Nullable String getUrl();

}
