package ru.korkmasov.tsc.bootstrap;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.korkmasov.tsc.api.service.IPropertyService;
import ru.korkmasov.tsc.component.LoggerExecutor;
import ru.korkmasov.tsc.service.PropertyService;

import static ru.korkmasov.tsc.constant.ActiveMQConst.URL;

public final class Bootstrap {

    public void start() {
        @NotNull final IPropertyService propertyService = new PropertyService();
        @NotNull final LoggerExecutor executor = new LoggerExecutor();
        @Nullable final String loggerURL = propertyService.getUrl();
        if (loggerURL == null || loggerURL.isEmpty())
            executor.logSingle(URL);
        else {
            @NotNull final String[] list = loggerURL.split(";");
            executor.log(list);
        }
    }

}
